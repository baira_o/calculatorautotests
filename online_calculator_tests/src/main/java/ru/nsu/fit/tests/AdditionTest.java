package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.NumericService;
import ru.nsu.fit.services.calculator.Calculator;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Arithmetic test. Addition. Input: keyboard")
public class AdditionTest extends TestScenarioBase{
    private static final String PAGE_URL = "http://testmethods.tmweb.ru/";

    @Title("Calculate integer addition")
    @Description("Check that Online Calculator right calculates integer addition (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition"})
    public void testSum() {
        String expression = "8 + 5";
        String expectedResult = "13";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("Calculate float addition")
    @Description("Check that Online Calculator right calculates float addition (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition"})
    public void testSumFloat() {
        String expression = "0.2 + 0.1";
        String expectedResult = "0.3";
        calculator.reset();
        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertTrue(NumericService.isEqual(result, expectedResult, Calculator.FLOAT_ACCURACY));
    }

    @Title("Calculate addition with zero")
    @Description("Check that Online Calculator right calculates addition with zero (input case - keyboard)")
    @Severity(SeverityLevel.MINOR)
    @Features({"Addition"})
    public void testSumZero() {
        String expression = "1 + 0";
        String expectedResult = "1";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("Calculate addition with infinity")
    @Description("Check that Online Calculator right calculates addition with infinity (input case - keyboard)")
    @Severity(SeverityLevel.MINOR)
    @Features({"Addition"})
    public void testSumInfinity() {
        String expression = "1 + Infinity";
        String expectedResult = "Infinity";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }

    @Title("Calculate addition with NaN")
    @Description("Check that Online Calculator right calculates addition with NaN (input case - keyboard)")
    @Severity(SeverityLevel.MINOR)
    @Features({"Addition"})
    public void testSumNaN() {
        String expression = "1 + NaN";
        String expectedResult = "NaN";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }

    @Title("UnaryAddition +a=")
    @Description("Unary addition")
    @Severity(SeverityLevel.MINOR)
    @Features({"Addition"})
    public void testUnaryMultiplication() {
        String expression = "+5";
        String expectedResult = "5";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }
}
