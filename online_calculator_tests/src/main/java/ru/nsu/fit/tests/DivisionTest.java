package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.NumericService;
import ru.nsu.fit.services.calculator.Calculator;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by user on 02.11.2015.
 */

@Title("Arithmetic test. Division")
public class DivisionTest extends TestScenarioBase{

    @Title("Calculate integer division")
    @Description("Check that Online Calculator right calculates integer division (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testIntegerDivision() {
        String expression = "9/2";
        String expectedResult = "4.5";

        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertTrue(NumericService.isEqual(result, expectedResult, Calculator.FLOAT_ACCURACY));
    }

    @Title("Calculate integer division")
    @Description("Check that Online Calculator right calculates integer division by one(input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testIntegerDivisionByOne() {
        String expression = "19/1";
        String expectedResult = "19";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("Calculate integer division")
    @Description("Check that Online Calculator right calculates integer division by itself(input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testIntegerDivisionByItself() {
        String expression = "19/19";
        String expectedResult = "1";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("Calculate integer division")
    @Description("Check that Online Calculator right calculates integer division zero by something (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testIntegerDivisionZeroBySomething() {
        String expression = "0/19";
        String expectedResult = "0";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("Calculate float devistion")
    @Description("Check that Online Calculator right calculates float division (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testFloatDivision() {
        String expression = "0.01 / 0.2";
        String expectedResult = "0.05";

        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertTrue(NumericService.isEqual(result, expectedResult, Calculator.FLOAT_ACCURACY));
    }

    @Title("Calculate division by zero")
    @Description("Check that Online Calculator right calculates division by zero (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testDivisionByZero() {
        String expression = "1 / 0";
        String expectedResult = "Infinity";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }

    @Title("Calculate division negative by zero")
    @Description("Check that Online Calculator right calculates division negative by zero (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testDivisionNegativeByZero() {
        String expression = "-1 / 0";
        String expectedResult = "-Infinity";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);

    }

    @Title("Calculate division zero by zero")
    @Description("Check that Online Calculator right calculates division zero by zero (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testDivisionZeroByZero() {
        String expression = "0 / 0";
        String expectedResult = "NaN";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }

    @Title("Calculate division by infinity")
    @Description("Check that Online Calculator right calculates division by infinity(input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testDivisionByInfinity() {
        String expression = "1 / Infinity";
        String expectedResult = "0";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }

    @Title("Calculate division Infinity by Infinity")
    @Description("Check that Online Calculator right calculates division inf by inf (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Division"})
    public void testDivisionInfByInf() {
        String expression = "Infinity / Infinity";
        String expectedResult = "NaN";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }

    @Title("UnaryDivision /a=")
    @Description("Unary division")
    @Severity(SeverityLevel.MINOR)
    @Features({"Division"})
    public void testUnaryDivision() {
        String expression = "/5";
        String expectedResult = "0";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }
}
