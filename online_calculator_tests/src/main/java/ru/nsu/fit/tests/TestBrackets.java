package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.calculator.Calculator;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by user on 02.11.2015.
 */

@Title("Test brackets")
public class TestBrackets extends TestScenarioBase {

    @Title("Calculate expression with correct brackets")
    @Description("Check that Online Calculator right calculates expression with correct brackets (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Brackets"})
    public void testExpressionWithBrackets() {
        String expression = "2*(1+3)-2";
        String expectedResult = "6";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }

    @Title("Calculate expression with invalid brackets")
    @Description("Check that Online Calculator right calculates expression with invalide brackets (input case - keyboard)")
    @Severity(SeverityLevel.MINOR)
    @Features({"Brackets"})
    public void testExpressionWithInvalidBrackets() {
        String expression = "2*(1+3))-2";
        String expectedResult = Calculator.ERROR_MESSAGE;

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
    }
}



