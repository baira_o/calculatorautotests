package ru.nsu.fit.tests;

import org.testng.Assert;
import ru.nsu.fit.services.NumericService;
import ru.nsu.fit.services.calculator.Calculator;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Baira on 09.11.2015.
 */
@Title("Multiplication test")
public class MultiplicationTest extends TestScenarioBase {

    @Title("Calculate float multiplication expression")
    @Description("Check that Online Calculator right calculates multiplication expression (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Multiplication"})
    public void testFloatMultiplication() {
        String expression = "0.1*0.1";
        String expectedResult = "0.01";

        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertTrue(NumericService.isEqual(result, expectedResult, Calculator.FLOAT_ACCURACY));
    }

    @Title("Multiplication commutativity")
    @Description("Check multiplication commutativity (input case - keyboard & mouse)")
    @Severity(SeverityLevel.NORMAL)
    @Features({"Multiplication"})
    public void testMultiplicationCommutativity() {
        String expressionL = "13*19";
        String expressionR = "19*13";
        String expectedResult = "247";

        String result1 = calculateExpressionUsingKeyboard(expressionL);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();

        String result2 = calculateExpressionUsingKeyboard(expressionR);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("Multiplication on 0")
    @Description("Check multiplication on 0 (input case - keyboard & mouse)")
    @Severity(SeverityLevel.MINOR)
    @Features({"Multiplication"})
    public void testMultiplicationByZero() {
        String expression1 = "0*1";
        String expectedResult = "0";

        String result1 = calculateExpressionUsingKeyboard(expression1);
        Assert.assertEquals(result1, expectedResult);

        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression1);
        Assert.assertEquals(result2, expectedResult);
        calculator.reset();

        String expression2 = "0*0";

        String result3 = calculateExpressionUsingKeyboard(expression2);
        Assert.assertEquals(result3, expectedResult);
    }

    @Title("Multiplication on 1")
    @Description("Check multiplication on 1 (input case - keyboard & mouse)")
    @Severity(SeverityLevel.MINOR)
    @Features({"Multiplication"})
    public void testMultiplicationByUnity() {
        String expression = "1*37";
        String expectedResult = "37";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("Multiplication a*b = infinity test")
    @Description("Multiplication a*b, where a*b > max allowed value")
    @Severity(SeverityLevel.MINOR)
    @Features({"Multiplication"})
    public void testMultiplicationInfinity() {
        String expression = buildExpression(MAX_INT_VALUE+1, 23, '*');
        String expectedResult = "Infinity";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }

    @Title("UnaryMultiplication *a=")
    @Description("UnaryMultiplication")
    @Severity(SeverityLevel.MINOR)
    @Features({"Multiplication"})
    public void testUnaryMultiplication() {
        String expression = "*5";
        String expectedResult = "0";

        String result1 = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result1, expectedResult);
        calculator.reset();
        String result2 = calculateExpressionUsingMouse(expression);
        Assert.assertEquals(result2, expectedResult);
    }
}
