package ru.nsu.fit.tests;

import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Baira on 09.11.2015.
 */
@Title("Copy paste test")
public class CopyPasteTest extends TestScenarioBase {

    @Title("Check paste feature")
    @Description("Check paste feature using keys ctrl+v)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Paste"})
    public void checkPasteUsingKeys() {
        String stringToBePaste = "1+2+3";
        String result = pasteFromClipboard(stringToBePaste);
        Assert.assertEquals(result, stringToBePaste);
    }

    @Title("Check copy feature")
    @Description("Check copy all from input using keys ctrl+c)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Copy"})
    public void checkCopyUsingKeys() {
        String toBeCopied = "1+2+3";
        typeExpressionUsingKeyboard(toBeCopied);
        String result = selectAllAndCopyToClipboard();
        Assert.assertEquals(result, toBeCopied);
    }

}
