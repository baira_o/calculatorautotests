package ru.nsu.fit.tests;

import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by user on 10.11.2015.
 */
public class TestUserScenarioPetya extends TestScenarioBase {
    @Title("Type expression using keyboard")
    @Description("Check that we can type text in input using keyboard")
    @Severity(SeverityLevel.CRITICAL)
    @Features("UI feature")
    public void testUserScenario() {
        String result;
        String expectedResult;
        calculateExpressionUsingKeyboard("(12 + 32) / 4 - 6 * 9");
        result = calculateExpressionUsingKeyboard("/ 5 + 14");
        expectedResult = "9";
        Assert.assertEquals(result, expectedResult);
    }
}
