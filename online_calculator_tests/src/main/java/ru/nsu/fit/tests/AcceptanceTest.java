package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.NumericService;
import ru.nsu.fit.services.calculator.Calculator;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Acceptance test")
public class AcceptanceTest extends TestScenarioBase {

    @Title("Open page")
    @Description("Check that we can open page with Online Calculator")
    @Severity(SeverityLevel.BLOCKER)
    @Features("UI feature")
    public void openPage() {
        // TODO: how to assert if page is really opened
        AllureUtils.saveTextLog("The page was opened successfully");
    }

    @Title("Type expression using keyboard")
    @Description("Check that we can type text in input using keyboard")
    @Severity(SeverityLevel.CRITICAL)
    @Features("UI feature")
    public void typeExpressionByKeyboard() {
        String inputString = "1234567890+-*/()";
        String result = typeExpressionUsingKeyboard(inputString);
        Assert.assertEquals(result, inputString);
    }


    @Title("Type expression using mouse")
    @Description("Check that we can type text in input using mouse")
    @Severity(SeverityLevel.CRITICAL)
    @Features("UI feature")
    public void typeExpressionByMouse() {
        String inputString = "1234567890+-*/";
        String result = typeExpressionUsingMouse(inputString);
        Assert.assertEquals(result, inputString);
    }

    @Test(dependsOnMethods = { "openPage", "typeExpressionByKeyboard" })
    @Title("Calculate some expression")
    @Description("Check that Online Calculator right calculates some expression (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"UI feature", "Subtraction", "Addition", "Multiplication", "Division"})
    public void calculateComplexExpression()  {
        final String expression = "(1 + 2) * 5 - 10 / 2 + 0.3 * 2";
        final String expectedResult = "10.6";

        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertTrue(NumericService.isEqual(result, expectedResult, Calculator.FLOAT_ACCURACY));
    }

    @Test(dependsOnMethods = "calculateComplexExpression")
    @Title("Calculate some float expression")
    @Description("Check that Online Calculator right calculates some float expression (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"UI feature", "Addition"})
    public void calculateFloatExpression() {
        final String expression = "-0.51984 + 0.2389";
        final String expectedResult = "-0.28094";

        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertTrue(NumericService.isEqual(result, expectedResult, Calculator.FLOAT_ACCURACY));
    }

}
