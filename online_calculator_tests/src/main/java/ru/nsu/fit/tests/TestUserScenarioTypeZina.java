package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by user on 10.11.2015.
 */

@Title("User scenario: Zina")
public class TestUserScenarioTypeZina extends TestScenarioBase{

    @Title("Type expression using keyboard")
    @Description("Check that we can type text in input using keyboard")
    @Severity(SeverityLevel.CRITICAL)
    @Features("UI feature")
    public void testUserScenario() {
        String result;
        String expectedResult;
        calculateExpressionUsingKeyboard("13350 - 13350 * 0.13");
        result = calculateExpressionUsingKeyboard("+ 5460 * 0.7");
        expectedResult = "15436.5";
        Assert.assertEquals(result, expectedResult);
        typeExpressionUsingKeyboard("\b\b\b\b\b\b\b");
        calculateExpressionUsingKeyboard("26950 - 26950 * 0.13");
        result = calculateExpressionUsingKeyboard("+ 4780 * 0.7");
        expectedResult = "26792.5";
        Assert.assertEquals(result, expectedResult);
    }
}
