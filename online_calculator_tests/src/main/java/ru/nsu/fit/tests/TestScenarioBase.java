package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.*;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.calculator.Calculator;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;

/**
 * Created by Baira on 31.10.2015.
 */
@Test
public abstract class TestScenarioBase {
    protected static final int MAX_INT_VALUE =  1000000000;
    protected static final int MIN_INT_VALUE = -1000000000;

    protected Calculator calculator;

    @BeforeClass
    public void setUp() {
        String browserName = "chrome";
        Browser browser = BrowserService.createInstance(browserName);
        calculator = new Calculator(browser);
        calculator.open();
    }

    @AfterClass
    public void tearDown() {
        calculator.close();
    }

    @BeforeMethod
    public void beforeTest() {
        calculator.reset();
        AllureUtils.saveImageAttach("Main screen [Before]",  calculator.makeScreenshot());
    }

    @AfterMethod
    public void afterTest() {
        calculator.reset();
    }

    @Step("Calculate expression {0} using keyboard.")
    protected String calculateExpressionUsingKeyboard(String expressionString) {
        String typedResult = typeExpressionUsingKeyboard(expressionString);

        String result = calculator.calculateExpression();
        AllureUtils.saveImageAttach("Result", calculator.makeScreenshot());
        return result;
    }

    @Step("Calculate expression {0} using mouse.")
    protected String calculateExpressionUsingMouse(String inputExpressionString) {
        String typedResult = typeExpressionUsingMouse(inputExpressionString);

        String result = calculator.calculateExpression();
        AllureUtils.saveImageAttach("Result", calculator.makeScreenshot());
        return result;
    }

    @Step("Typing expression {0} using mouse.")
    protected String typeExpressionUsingMouse(String expression)
    {
        String result = calculator.typeExpressionUsingMouse(expression);
        AllureUtils.saveImageAttach("The expression is typed using mouse", calculator.makeScreenshot());
        return result;
    }

    @Step("Typing expression {0} using keyboard")
    protected String typeExpressionUsingKeyboard(String expression)
    {
        String result = calculator.typeExpressionUsingKeyboard(expression);
        AllureUtils.saveImageAttach("The expression is typed using keyboard", calculator.makeScreenshot());
        return result;
    }

    @Step("Select all from input field and copy to clipboard")
    protected String selectAllAndCopyToClipboard() {
        calculator.copyFromInputUsingKeys();
        AllureUtils.saveImageAttach("Select all and copy", calculator.makeScreenshot());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            return (String) clipboard.getData(DataFlavor.stringFlavor);
        }
        catch (Exception exc)
        {
            return exc.toString();
        }
    }

    @Step("Paste from clipboard to input field {0}")
    protected String pasteFromClipboard(String stringToBePaste) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection str = new StringSelection(stringToBePaste);
        clipboard.setContents(str, null);

        String result = calculator.pasteToInputUsingKeys();
        AllureUtils.saveImageAttach("Paste to input field", calculator.makeScreenshot());
        return result;
    }

    @Step("Clear input (select all, backspace)")
    protected String clearInputUsingKeys() {
        AllureUtils.saveImageAttach("Before clear input", calculator.makeScreenshot());
        String result = calculator.clearInputUsingKeys();
        AllureUtils.saveImageAttach("After clear input", calculator.makeScreenshot());
        return result;
    }

    @Step("Press backspace")
    protected String pressBackspace() {
        AllureUtils.saveImageAttach("Before press backspace", calculator.makeScreenshot());
        String result = calculator.pressBackspace();
        AllureUtils.saveImageAttach("After press backspace", calculator.makeScreenshot());
        return result;
    }
    protected String buildExpression(int a, int b, char operation) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(a);
        stringBuilder.append(operation);
        stringBuilder.append(b);
        return stringBuilder.toString();
    }
}
