package ru.nsu.fit.tests;

import org.testng.Assert;
import ru.nsu.fit.services.NumericService;
import ru.nsu.fit.services.calculator.Calculator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Baira on 10.11.2015.
 */
public class FirstUserScenarioTest extends TestScenarioBase {

    @Title("First User Scenario")
    @Description("Check First User Scenario")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"UI feature", "Addition", "Multiplication", "Division", "Subtraction", ""})
    public void testFirstUserScenario()
    {
        final String expressionToBePaste1 = "-(-5.25 + 5.26)/10.00 + 2.2 *2015";

        String pasteResult = pasteFromClipboard(expressionToBePaste1);
        Assert.assertEquals(pasteResult, expressionToBePaste1);

        String calculationResult = calculator.calculateExpression();
        Assert.assertTrue(NumericService.isEqual(calculationResult, "4432.999", Calculator.FLOAT_ACCURACY));

        String copiedResult = selectAllAndCopyToClipboard();
        Assert.assertEquals(copiedResult, calculationResult);

        String clearedInputResult = clearInputUsingKeys();
        Assert.assertEquals(clearedInputResult, "");

        final String expression2 = "89.99+0.001";
        final String expression3 = "1";
        final String expression23 = "89.99+0.01";

        String typedExpression2 = typeExpressionUsingKeyboard(expression2);
        Assert.assertEquals(typedExpression2, expression2);

        String afterBackspace1 = pressBackspace();
        Assert.assertEquals(afterBackspace1, expression2.substring(0, expression2.length()-1));

        String afterBackspace2 = pressBackspace();
        Assert.assertEquals(afterBackspace2, expression2.substring(0, expression2.length()-2));

        String typedExpression3 = typeExpressionUsingKeyboard(expression3);
        Assert.assertEquals(typedExpression3, expression23);

        String calculationResult2 = calculator.calculateExpression();

        Assert.assertTrue(NumericService.isEqual(calculationResult2, "90", Calculator.FLOAT_ACCURACY));
    }
}
