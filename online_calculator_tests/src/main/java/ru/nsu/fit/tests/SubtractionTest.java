package ru.nsu.fit.tests;

import org.testng.Assert;
import ru.nsu.fit.services.NumericService;
import ru.nsu.fit.services.calculator.Calculator;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Baira on 09.11.2015.
 */
@Title("Subtraction test")

public class SubtractionTest extends TestScenarioBase {

    @Title("Calculate integer subtraction expression")
    @Description("Check that Online Calculator right calculates subtraction expression (input case - keyboard & mouse)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Subtraction"})
    public void testIntegerSubtraction() {
        String expression1 = "100-1";
        String expectedResult1 = "99";

        String result1 = calculateExpressionUsingKeyboard(expression1);
        Assert.assertEquals(result1, expectedResult1);
        calculator.reset();
        String expression2 = "1-100";
        String expectedResult2 = "-99";

        String result2 = calculateExpressionUsingKeyboard(expression2);
        Assert.assertEquals(result2, expectedResult2);
    }

    @Title("Calculate float subtraction expression")
    @Description("Check that Online Calculator right calculates subtraction expression (input case - keyboard)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Subtraction"})
    public void testFloatSubtraction() {
        String expression = "0.3-0.1";
        String expectedResult = "0.2";

        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertTrue(NumericService.isEqual(result, expectedResult, Calculator.FLOAT_ACCURACY));
    }

    @Title("UnarySubtraction -a=")
    @Description("UnarySubtraction")
    @Severity(SeverityLevel.MINOR)
    @Features({"Subtraction"})
    public void testUnarySubtraction() {
        String expression = "-5";
        String expectedResult = "-5";

        String result = calculateExpressionUsingKeyboard(expression);
        Assert.assertEquals(result, expectedResult);
    }
}
