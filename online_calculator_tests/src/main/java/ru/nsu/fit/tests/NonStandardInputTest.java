package ru.nsu.fit.tests;

import org.testng.Assert;
import ru.nsu.fit.services.calculator.Calculator;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Baira on 10.11.2015.
 */
public class NonStandardInputTest extends TestScenarioBase {
    /*
    @Title("Nonstandard input")
    @Description("Check ")
    @Severity(SeverityLevel.MINOR)
    @Features({"Keys"})
    public void checkErrorMessage1() {
        String inputString = "alert(\"Hello, world\");";
        String result = calculateExpressionUsingKeyboard(inputString);
        Assert.assertEquals(result, Calculator.ERROR_MESSAGE);
    }
    */
    @Title("Nonstandard input")
    @Description("Check ")
    @Severity(SeverityLevel.MINOR)
    @Features({"Keys"})
    public void checkErrorMessage2() {
        String inputString = "5+5//";
        String result = calculateExpressionUsingKeyboard(inputString);
        Assert.assertEquals(result, Calculator.ERROR_MESSAGE);
    }
    @Title("Nonstandard input")
    @Description("Check ")
    @Severity(SeverityLevel.MINOR)
    @Features({"Keys"})
    public void checkErrorMessage3() {
        String inputString = "10^7";
        String result = calculateExpressionUsingKeyboard(inputString);
        Assert.assertEquals(result, Calculator.ERROR_MESSAGE);
    }
}
