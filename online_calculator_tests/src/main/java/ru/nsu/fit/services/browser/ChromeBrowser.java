package ru.nsu.fit.services.browser;

import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

/**
 * Created by Baira on 09.11.2015.
 */
public class ChromeBrowser extends Browser {
     public ChromeBrowser()
    {
        try {
            // TODO: move to settings
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            webDriver = new ChromeDriver();
            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
