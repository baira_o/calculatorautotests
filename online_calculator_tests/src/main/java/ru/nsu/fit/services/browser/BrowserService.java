package ru.nsu.fit.services.browser;

public class BrowserService {
    public static Browser createInstance(String browserName) {
        if (browserName.toLowerCase().contains("firefox")) {
            return new FirefoxBrowser();
        }
        if (browserName.toLowerCase().contains("chrome")) {
            return new ChromeBrowser();
        }
        return new FirefoxBrowser();
    }
}
