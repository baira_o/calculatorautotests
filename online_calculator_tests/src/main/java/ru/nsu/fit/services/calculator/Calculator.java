package ru.nsu.fit.services.calculator;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import ru.nsu.fit.services.browser.Browser;

import java.io.Closeable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Baira on 09.11.2015.
 */
public class Calculator implements Closeable {
    public static final String ERROR_MESSAGE = "Error";
    public static final String INFINITY = "Infinity";
    public static final String NAN = "NaN";
    public static final double FLOAT_ACCURACY = 0.001;

    private static final String PAGE_URL = "http://testmethods.tmweb.ru/";
    private static final By inputElement = By.xpath("//input[@type='text' and @name='Input']");
    private static final By equalElement = By.xpath("//input[@type='button' and @name='DoIt']");
    private static final By clearElement = By.xpath("//input[@type='button' and @name='clear']");
    private static final Map<Character, By> buttonElements;

    static {
        Map<Character, By> buttons = new HashMap();
        buttons.put('0', By.xpath("//input[@type='button' and @name='zero']"));
        buttons.put('1', By.xpath("//input[@type='button' and @name='one']"));
        buttons.put('2', By.xpath("//input[@type='button' and @name='two']"));
        buttons.put('3', By.xpath("//input[@type='button' and @name='three']"));
        buttons.put('4', By.xpath("//input[@type='button' and @name='four']"));
        buttons.put('5', By.xpath("//input[@type='button' and @name='five']"));
        buttons.put('6', By.xpath("//input[@type='button' and @name='six']"));
        buttons.put('7', By.xpath("//input[@type='button' and @name='seven']"));
        buttons.put('8', By.xpath("//input[@type='button' and @name='eight']"));
        buttons.put('9', By.xpath("//input[@type='button' and @name='nine']"));
        buttons.put('+', By.xpath("//input[@type='button' and @name='plus']"));
        buttons.put('-', By.xpath("//input[@type='button' and @name='minus']"));
        buttons.put('*', By.xpath("//input[@type='button' and @name='times']"));
        buttons.put('/', By.xpath("//input[@type='button' and @name='div']"));
        buttonElements = Collections.unmodifiableMap(buttons);
    }

    private Browser browser;

    public Calculator(Browser browser)
    {
        this.browser = browser;
    }

    public void open() {
        browser.openPage(PAGE_URL);
    }

    public void reset() {
        browser.clearElement(inputElement);
    }

    public String calculateExpression()
    {
        browser.click(equalElement);
        return browser.getValue(inputElement);
    }

    public String typeExpressionUsingKeyboard(String expression)
    {
        browser.typeText(inputElement, expression);
        return browser.getValue(inputElement);
    }

    public String typeExpressionUsingMouse(String expression)
    {
        final String inputText = prepareInputString(expression);
        for (int i = 0; i < inputText.length(); i++) {
            if (buttonElements.containsKey(inputText.charAt(i))) {
                By element = buttonElements.get(inputText.charAt(i));
                browser.click(element);
            }
        }
        return prepareInputString(browser.getValue(inputElement));
    }

    public String clearInputUsingMouse()
    {
        browser.click(clearElement);
        return browser.getValue(inputElement);
    }

    public String clearInputUsingKeys()
    {
        browser.getElement(inputElement).sendKeys(Keys.LEFT_CONTROL, "a");
        browser.getElement(inputElement).sendKeys(Keys.BACK_SPACE);
        return browser.getValue(inputElement);
    }

    public String pressBackspace()
    {
        browser.getElement(inputElement).sendKeys(Keys.BACK_SPACE);
        return browser.getValue(inputElement);
    }

    public String copyFromInputUsingKeys()
    {
        browser.copyAllToClipboardUsingKeys(inputElement);
        return browser.getValue(inputElement);
    }

    public String pasteToInputUsingKeys()
    {
        browser.pasteFromClipboardUsingKeys(inputElement);
        return browser.getValue(inputElement);
    }

    public byte[] makeScreenshot() {
        return browser.makeScreenshot();
    }

    private static String prepareInputString(String string) {
        return string.replaceAll("\\s+","");
    }

    @Override
    public void close() {
        browser.close();
    }
}
