package ru.nsu.fit.services.browser;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.nsu.fit.shared.ImageUtils;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

public abstract class Browser implements Closeable {
    protected WebDriver webDriver;

    public Browser openPage(String url) {
        webDriver.get(url);
        return this;
    }

    public Browser waitForElement(By element) {
        return waitForElement(element, 10);
    }

    public Browser waitForElement(final By element, int timeoutSec) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeoutSec);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        return this;
    }

    public Browser click(By element) {
        webDriver.findElement(element).click();
        return this;
    }

    public Browser typeText(By element, String text) {
        webDriver.findElement(element).sendKeys(text);
        return this;
    }

    public Browser clearElement(By element) {
        webDriver.findElement(element).clear();
        return this;
    }

    public Browser copyAllToClipboardUsingKeys(By fromElement)
    {
        WebElement fromWebElement = getElement(fromElement);
        fromWebElement.sendKeys(Keys.LEFT_CONTROL, "a");
        fromWebElement.sendKeys(Keys.LEFT_CONTROL, "c");
        fromWebElement.sendKeys(Keys.END);

        return this;
    }

    public Browser pasteFromClipboardUsingKeys(By toElement)
    {
        getElement(toElement).sendKeys(Keys.LEFT_CONTROL, "v");
        return this;
    }

    public WebElement getElement(By element) {
        return webDriver.findElement(element);
    }

    public String getValue(By element) {
        return getElement(element).getAttribute("value");
    }

    public List<WebElement> getElements(By element) {
        return webDriver.findElements(element);
    }

    public boolean isElementPresent(By element) {
        return getElements(element).size() != 0;
    }

    public byte[] makeScreenshot() {
        try {
            return ImageUtils.toByteArray(((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void close() {
        webDriver.close();
    }
}
