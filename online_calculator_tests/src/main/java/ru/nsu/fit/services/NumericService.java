package ru.nsu.fit.services;

/**
 * Created by Baira on 10.11.2015.
 */
public class NumericService {
    public static Integer tryParseInt(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static Double tryParseDouble(String text) {
        try {
            return Double.parseDouble(text);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static boolean isEqual(double a, double b, double epsilon)
    {
        return Math.abs(a - b) <= epsilon;
    }

    public static boolean isEqual(String numA, String numB, double epsilon)
    {
        Double a = tryParseDouble(numA);
        Double b = tryParseDouble(numB);

        if (a!=null && b!= null)
        {
            return isEqual(a.doubleValue(), b.doubleValue(), epsilon);
        }

        return a == b;
    }
}
