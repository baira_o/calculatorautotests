package ru.nsu.fit.services.browser;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.concurrent.TimeUnit;

/**
 * Created by Baira on 09.11.2015.
 */
public class FirefoxBrowser extends Browser {
    public FirefoxBrowser()
    {
        // create profile
        FirefoxProfile profile = new FirefoxProfile();
        // create web driver
        try {
            webDriver = new FirefoxDriver(profile);
            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
